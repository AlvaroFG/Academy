# Grademy

Grademy is a multiplatform (Android, iOS, Web) hybrid application for academic management, both for employees and students

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

You need to install npm for packaging and dependencies management and ionic-angular client

### Installing

For installing Ionic-Angular client go to official documentation: 

https://ionicframework.com/docs/intro/installation/

## Deployment

Web deployment: <br />
    cd {ProjectDirectory} <br />
    ionic serve <br />

Android deployment: <br />
    cd {ProjectDirectory} <br />
    ionic cordova build android <br />

iOS deployment: <br />
    cd {ProjectDirectory} <br />
    ionic cordova build ios <br />

## Built With

* [Ionic](https://ionicframework.com/)
* [Angular](https://angular.io/)
* [Cordova](https://cordova.apache.org/)
* [npm](https://www.npmjs.com/)

## Authors

* **Álvaro Fernández** - *Initial work* - [AlvaroFG](https://gitlab.com/AlvaroFG)

## License

This project is open source
