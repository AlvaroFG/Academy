import { Subject } from './subject.model';

export interface Roles {
    reader: boolean;
    author?: boolean;
    admin?: boolean;
}

export class User {
    email: string;
    roles: Roles;
    name: string;
    lastname: string;
    birthdate?: string;
    profilePicture?: string;
    subjects?: Array<Subject>;
    deleted?: boolean;
    key?: string;

    constructor(authData) {
        this.email = authData.email;
        this.roles = { reader: true }
        this.name = authData.name;
        this.lastname = authData.lastname;
        this.birthdate = authData.birthdate;
        this.subjects = new Array<Subject>();
    }
}