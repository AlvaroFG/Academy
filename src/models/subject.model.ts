export class Subject {
    name: string;
    course: number;
    type: string;
    speciality?: string;
    deleted?: boolean;

    constructor(data) {
        this.name = data.name;
        this.course = data.course;
        this.type = data.type;
        this.speciality = data.speciality;
    }
}