import { Component } from '@angular/core';
import { NavController, LoadingController, Loading, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AuthProvider } from '../../providers/auth/auth';
import { UserService } from '../../services/user.service';

import { TabsComponent } from '../tabs/tabs.component';
import { StudentTabsComponent } from '../student-tabs/student-tabs.component';
import { ResetPasswordComponent } from '../reset-password/reset-password.component';
import { SignupComponent } from '../signup/signup.component';
import { EmailValidator } from '../../validators/email.validator';

@Component({
  selector: 'page-login',
  templateUrl: 'login.component.html'
})
export class LoginComponent {
  loginForm: FormGroup;
  loading: Loading;

  constructor(
    public navController: NavController,
    public authProvider: AuthProvider,
    public formBuilder: FormBuilder,
    public alertController: AlertController,
    public loadingController: LoadingController,
    public userService: UserService
  ) {
    this.loginForm = formBuilder.group({
      email: ['', Validators.compose([Validators.required,
      EmailValidator.isValid])],
      password: ['', Validators.compose([Validators.minLength(6),
      Validators.required])]
    });
  }

  goToResetPassword() {
    this.navController.push(ResetPasswordComponent);
  }

  createAccount() {
    this.navController.push(SignupComponent);
  }

  loginUser() {
    if (!this.loginForm.valid) {
      console.log(this.loginForm.value);
    }
    else {
      this.authProvider.loginUser(this.loginForm.value.email, this.loginForm.value.password)
        .then(authProvider => {
          this.userService.getUser(this.loginForm.value.email)
            .subscribe(users => {
                var currentUser = users[0];
                if (!currentUser.roles.admin == undefined || !currentUser.roles.admin) {
                  this.navController.setRoot(StudentTabsComponent);
                }
                else {
                  this.navController.setRoot(TabsComponent);
                }
              },
              (error) => {
                console.log(error.message);
              }
            );
        }, error => {
          this.loading.dismiss().then(() => {
            let alert = this.alertController.create({
              message: error.message,
              buttons: [
                {
                  text: 'Ok',
                  role: 'cancel'
                }
              ]
            });
            alert.present();
          });
        });

      this.loading = this.loadingController.create({
        dismissOnPageChange: true
      });
      this.loading.present();
    }
  }
}