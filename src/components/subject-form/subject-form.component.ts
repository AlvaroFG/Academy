import { Component } from '@angular/core';
import { Subject } from '../../models/subject.model';
import { NavController, AlertController } from 'ionic-angular';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SubjectService } from '../../services/subject.service';
import { NavParams } from 'ionic-angular/navigation/nav-params';

@Component({
    selector: "page-subject-form",
    templateUrl: "subject-form.component.html"
})
export class SubjectFormComponent {
    private readonly newSubjectTitle: string = "Nueva asignatura";
    private readonly editedSubjectTitle: string = "Modificar asignatura";

    private editing: boolean;
    private key: string;
    private editedSubject: Subject;
    
    title: string;
    subjectForm: FormGroup;

    constructor(
        public navController: NavController,
        public navParams: NavParams,
        public alertController: AlertController,
        public subjectService: SubjectService
    ) {
        this.editing = this.navParams.get("editing");
        if (this.editing) {
            this.title = this.editedSubjectTitle;
            this.loadEditedSubject();
        }
        else {
            this.title = this.newSubjectTitle;
            this.subjectForm = new FormGroup({
                name: new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(45)]),
                course: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(1)]),
                type: new FormControl('EPO', [Validators.required]),
                speciality: new FormControl('', [Validators.maxLength(30)])
            });
        }
    }

    saveSubject() : void {
        var subjectData = {
            name: this.subjectForm.value.name,
            course: this.subjectForm.value.course,
            type: this.subjectForm.value.type,
            speciality: this.subjectForm.value.speciality
        }

        var subject: Subject = new Subject(subjectData);

        if (!this.editing) {
            this.subjectService.postSubject(subject).then(saved => {
                let alert = this.alertController.create({
                    message: "Asignatura creada correctamente",
                    buttons: [{
                        text: 'Ok',
                        role: 'cancel'
                    }]
                });
                alert.onDidDismiss(dismissed => {
                    this.navController.pop();
                });
                alert.present();
            });
        }
        else {
            this.subjectService.updateSubject(this.key, subject).then(updated => {
                let alert = this.alertController.create({
                    message: "Asignatura modificada correctamente",
                    buttons: [{
                        text: 'Ok',
                        role: 'cancel'
                    }]
                });
                alert.onDidDismiss(dismissed => {
                    this.navController.pop();
                });
                alert.present();
            });
        }
    }

    private loadEditedSubject() : void {
        this.key = this.navParams.get("key");
        console.log(this.key);
        this.editedSubject = this.navParams.get("subject");
        this.subjectForm = new FormGroup({
            name: new FormControl(this.editedSubject.name, [Validators.required, Validators.minLength(5), Validators.maxLength(25)]),
            course: new FormControl(this.editedSubject.course, [Validators.required, Validators.minLength(1), Validators.maxLength(1)]),
            type: new FormControl(this.editedSubject.type, [Validators.required]),
            speciality: new FormControl(this.editedSubject.speciality, [Validators.maxLength(20)])
        });
    }
}
