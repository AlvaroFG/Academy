import { Component } from '@angular/core';
import { UserListComponent } from '../../components/tabs-components/users-list/users-list.component';
import { SubjectListComponent } from '../../components/tabs-components/subjects-list/subjects-list.component';

@Component({
  templateUrl: 'tabs.component.html'
})
export class TabsComponent {

  tab1Root = UserListComponent;
  tab2Root = SubjectListComponent;

  constructor() {
  }
}
