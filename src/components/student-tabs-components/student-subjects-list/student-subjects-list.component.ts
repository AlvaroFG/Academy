import { Component } from '@angular/core';
import { AuthProvider } from '../../../providers/auth/auth';
import { UserService } from '../../../services/user.service';
import { App, NavController } from 'ionic-angular';
import { LoginComponent } from '../../login/login.component';
import { SubjectDetailComponent } from '../../student-tabs-components/subject-detail/subject-detail.component';
import { Subject } from '../../../models/subject.model';
import { User } from '../../../models/user.model';
import { SubjectConstants } from '../../../common/subject-constants';

@Component({
    selector: 'page-subjects-list',
    templateUrl: 'student-subjects-list.component.html'
})
export class StudentSubjectListComponent {
    subjects: Array<Subject>;
    currentUser: string;

    constructor(
        public authProvider: AuthProvider,
        public userService: UserService,
        public navController: NavController,
        public app: App
    ) {
        this.subjects = new Array<Subject>();
        this.currentUser = this.authProvider.getLoggedUser();
        this.userService.getUser(this.currentUser).subscribe(users => {
            let user: User = users[0];
            this.subjects = user.subjects;
        });
    }
    
    detail(subject: Subject): void {
        this.navController.push(
            SubjectDetailComponent,
            {
                subject: subject
            });
    }

    getType(type: string) : string {
        return SubjectConstants.getValue(type);
    }
}