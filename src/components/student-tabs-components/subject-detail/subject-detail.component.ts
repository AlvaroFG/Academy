import { Component } from '@angular/core';
import { Subject } from '../../../models/subject.model';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { SubjectConstants } from '../../../common/subject-constants';

@Component({
    selector: "page-subject-detail",
    templateUrl: "subject-detail.component.html"
})
export class SubjectDetailComponent {
    private subject: Subject;

    constructor(public navParams: NavParams) {
        this.subject = navParams.get("subject");
    }

    getType(type: string) {
        return SubjectConstants.getValue(type);
    }
}