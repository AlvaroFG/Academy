import { Component } from '@angular/core';
import { AlertController, Platform, ToastController } from 'ionic-angular';
import { ImagePicker } from '@ionic-native/image-picker';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { AuthProvider } from '../../../providers/auth/auth';
import { Diagnostic } from '@ionic-native/diagnostic';
import { UserService } from '../../../services/user.service';
import { User } from '../../../models/user.model';

@Component({
    selector: "page-student-profile",
    templateUrl: "student-profile.component.html"
})
export class StudentProfileComponent {
    private readonly Quality: number = 30;
    private user: User;
    profilePicture: string;

    constructor(
        private imagePicker: ImagePicker,
        private camera: Camera,
        private diagnostic: Diagnostic,
        public userService: UserService,
        public authProvider: AuthProvider,
        public platform: Platform,
        public alertController: AlertController,
        public toastController: ToastController
    ) {
        this.user = new User({ name: "", lastname: "", email: "" });
        this.userService.getUser(authProvider.getLoggedUser())
            .subscribe(users => {
                this.user = users[0];
                if (this.user.profilePicture !== undefined) {
                    this.profilePicture = this.user.profilePicture;
                }
            });
        this.camera.cleanup();
    }

    galleryPicking() {
        let imagePickerOptions = {
            maximumImagesCount: 1,
            outputType: 1, // String base 64
            quality: this.Quality
        };

        if (this.imagePicker.hasReadPermission()) {
            this.imagePicker.getPictures(imagePickerOptions).then((results) => {
                let result = results[0];
                if (result !== undefined && result !== null) {
                    this.profilePicture = result;
                    let toast = this.toastController.create({
                        message: "Imagen seleccionada",
                        duration: 2000,
                        position: 'middle'
                    });
                    toast.present();
                }
            });
        }
        else {
            let alert = this.alertController.create({
                title: "Galería de imágenes",
                message: "La aplicación no tiene permiso para acceder a la galería. Revise su configuración, por favor.",
                buttons: [{
                    text: "Ok",
                    role: 'cancel'
                }]
            });
            alert.present();
        }
    }

    cameraPicking() {
        if (this.diagnostic.isCameraAvailable && this.diagnostic.isCameraAuthorized) {

            let cameraOptions: CameraOptions = {
                quality: this.Quality,
                targetHeight: 300,
                targetWidth: 300,
                destinationType: this.camera.DestinationType.DATA_URL,
                encodingType: this.camera.EncodingType.PNG,
                mediaType: this.camera.MediaType.PICTURE,
                sourceType: this.camera.PictureSourceType.CAMERA
            };
            this.camera.getPicture(cameraOptions).then((imageData) => {

                if (imageData !== null && imageData !== undefined) {
                    this.profilePicture = imageData;
                    let toast = this.toastController.create({
                        message: "Imagen seleccionada",
                        duration: 2000,
                        position: 'middle'
                    });
                    toast.present();
                }

            }, (error) => {
                let toast = this.toastController.create({
                    message: error.message,
                    duration: 4000,
                    position: 'middle'
                });
                toast.present();
            });
        }
        else {
            let alert = this.alertController.create({
                title: "La app no tiene acceso a la cámara",
                message: "Revise la configuración de su dispositivo"
            });
            alert.present();
        }
    }

    fileChange(event) {
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            let file: File = fileList[0];
            var reader = new FileReader();
            reader.onload = this._handleReaderLoaded.bind(this);
            reader.readAsBinaryString(file);
        }
    }

    isBrowser(): boolean {
        return this.platform.is('core') || this.platform.is('mobileweb') ? true : false;
    }

    update() {
        if (this.profilePicture !== undefined && this.profilePicture !== null) {
            this.user.profilePicture = this.profilePicture;

            this.userService.updateUser(this.user.key, this.user);
            let toast = this.toastController.create({
                message: "Imagen de perfil actualizada",
                duration: 2000,
                position: 'middle'
            });
            toast.present();
        }
        else {
            let toast = this.toastController.create({
                message: "Selecciona antes una imagen",
                duration: 3000,
                position: 'middle'
            });
            toast.present();
        }
    }

    private _handleReaderLoaded(readerEvt) {
        var binaryString = readerEvt.target.result;
        this.profilePicture = btoa(binaryString);  // Converting binary string data.
    }
}