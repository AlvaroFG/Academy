import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthProvider } from '../../providers/auth/auth';
import { EmailValidator } from '../../validators/email.validator';

@Component({
    selector: 'page-reset-password',
    templateUrl: 'reset-password.component.html'
})
export class ResetPasswordComponent {
    public resetPasswordForm: FormGroup;

    constructor(
        public authProvider: AuthProvider,
        public formBuilder: FormBuilder,
        public navController: NavController,
        public alertController: AlertController
    ) {
        this.resetPasswordForm = formBuilder.group({
            email: ['', Validators.compose([Validators.required, EmailValidator.isValid])]
        });
    }

    resetPassword() {
        if (!this.resetPasswordForm.valid) {
            console.log(this.resetPasswordForm.value);
        }
        else {
            this.authProvider.resetPassword(this.resetPasswordForm.value.email)
                .then((user) => {
                    let alert = this.alertController.create({
                        message: "Acabamos de enviar a tu email un link para restablecer tu contraseña",
                        buttons: [{
                            text: "Ok",
                            role: 'cancel',
                            handler: () => {
                                this.navController.pop();
                            }
                        }]
                    });
                    alert.present();
                }, (error) => {
                    var errorMessage: string = error.message;
                    let errorAlert = this.alertController.create({
                        message: errorMessage,
                        buttons: [{
                            text: "Ok",
                            role: 'cancel'
                        }]
                    });
                    errorAlert.present();
                });
        }
    }
}