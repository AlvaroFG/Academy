import { Component } from '@angular/core';
import { AuthProvider } from '../../../providers/auth/auth';
import { SubjectService } from '../../../services/subject.service';
import { App, NavController } from 'ionic-angular';
import { SubjectFormComponent } from '../../subject-form/subject-form.component';
import { Subject } from '../../../models/subject.model';
import { SubjectConstants } from '../../../common/subject-constants';

@Component({
    selector: 'page-subjects-list',
    templateUrl: 'subjects-list.component.html'
})
export class SubjectListComponent {
    subjects: Array<Subject>;
    currentUser: string;

    constructor(
        public authProvider: AuthProvider,
        public subjectService: SubjectService,
        public app: App,
        public navController: NavController
    ) {
        this.subjects = subjectService.getSubjects();
        this.currentUser = this.authProvider.getLoggedUser();
    }

    newSubject() : void {
        this.navController.push(SubjectFormComponent);
    }

    edit(key: string, subject: Subject): void {
        this.navController.push(
            SubjectFormComponent,
            {
                editing: true,
                key: key,
                subject: subject
            });
    }

    deleteSubject(key: string, subject: Subject) : void {
        this.subjectService.deleteSubject(key, subject);
    }

    getType(type: string) : string {
        return SubjectConstants.getValue(type);
    }

    onSearch(event: any) {
        this.initializeItems();
        let val = event.target.value;
        if (val && val.trim() != '') {
            this.subjects = this.subjectService.getSubjectsByName(val);
        }
    }

    onSearchCancel(event: any) {
        this.initializeItems();
    }

    initializeItems() {
        this.subjects = this.subjectService.getSubjects();
    }
}