import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { AngularFireList } from 'angularfire2/database';
import { UserService } from '../../../../services/user.service';
import { SubjectService } from '../../../../services/subject.service';
import { User } from '../../../../models/user.model';
import { Subject } from '../../../../models/subject.model';
import { SubjectConstants } from '../../../../common/subject-constants';
import { FabContainer } from 'ionic-angular/components/fab/fab-container';

@Component({
    selector: 'page-user-edit',
    templateUrl: 'user-edit.component.html'
})
export class UserEditComponent {
    private selectedType: string;
    private key: string
    private storedUser: User;

    private storedSubjects: Array<Subject>;
    private userSelectedSubjects: Array<Subject>;

    constructor(
        public navController: NavController,
        public navParams: NavParams,
        public alertController: AlertController,
        public subjectService: SubjectService,
        public userService: UserService
    ) {
        this.initializeModifiedUser();
        this.storedSubjects = subjectService.getSubjectsFiltered(SubjectConstants.ESO);
    }

    updateSubjects() {
        this.storedUser.subjects = this.userSelectedSubjects;
        this.userService.updateUser(this.key, this.storedUser)
            .then(updated => {
                 let alert = this.alertController.create({
                     message: 'Alumno modificado correctamente',
                     buttons: [{
                         text: 'Ok',
                         role: 'cancel'
                     }]
                 });
                 alert.onDidDismiss(() => {
                     this.navController.pop();
                 });
                 alert.present();
            });
    }

    private selected(selected: string, fab: FabContainer) {
        switch (selected)
        {
            case SubjectConstants.ESO:
                this.selectedType = SubjectConstants.EsoValue;
                this.storedSubjects = this.subjectService.getSubjectsFiltered(SubjectConstants.ESO);
                break;
            case SubjectConstants.BACCALAURATE:
                this.selectedType = SubjectConstants.BaccalaurateValue;
                this.storedSubjects = this.subjectService.getSubjectsFiltered(SubjectConstants.BACCALAURATE);
                break;
            case SubjectConstants.GRADE:
                this.selectedType = SubjectConstants.GradeValue;
                this.storedSubjects = this.subjectService.getSubjectsFiltered(SubjectConstants.GRADE);
                break;
            case SubjectConstants.POSTGRADE:
                this.selectedType = SubjectConstants.PostgradeValue;
                this.storedSubjects = this.subjectService.getSubjectsFiltered(SubjectConstants.POSTGRADE);
                break;
        }
        fab.close();
    }

    private add(subject: Subject) {
        if (!this.userSelectedSubjects
                .find(s => s.name == subject.name &&
                    s.speciality == subject.speciality)) {

                        var newSubject: Subject = new Subject({
                            name: subject.name,
                            course: subject.course,
                            type: subject.type, 
                            speciality: subject.speciality,
                            checked: true
                         });
                         this.userSelectedSubjects.push(newSubject);
                    }
    }

    private remove(subject: Subject) {
        let index: number = this.userSelectedSubjects.indexOf(subject);
        this.userSelectedSubjects.splice(index, 1);
    }

    private initializeModifiedUser() {
        this.selectedType = "ESO";
        this.key = this.navParams.get("key");
        this.storedUser = this.navParams.get("user");
        this.userSelectedSubjects = new Array<Subject>();

        if (this.storedUser.subjects !== undefined) {
            this.storedUser.subjects.forEach(subject => {
                this.userSelectedSubjects.push(subject);
            })
        }
    }
}