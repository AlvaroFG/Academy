import { Component } from '@angular/core';
import { AuthProvider } from '../../../providers/auth/auth';
import { UserService } from '../../../services/user.service';
import { App, NavController } from 'ionic-angular';
import { UserEditComponent } from './user-edit/user-edit.component';
import { User } from '../../../models/user.model';

@Component({
    selector: 'page-user-list',
    templateUrl: 'users-list.component.html'
})
export class UserListComponent {
    private readonly Name: string = "name";
    private readonly Lastname: string = "lastname";
    private readonly Email: string = "email";
    private selectOptions;
    private filter: string = "name";
    users: Array<User>;
    currentUser: string;

    constructor(
        public authProvider: AuthProvider,
        public userService: UserService,
        public app: App,
        public navController: NavController
    ) {
        this.initializeItems();
        this.currentUser = this.authProvider.getLoggedUser();

        this.selectOptions = {
            title: 'Filtro de búsqueda'
          };
    }

    delete(user) {
        this.userService.deleteUser(user.key, user).then(deleted => {
            console.log("deleted " + user.email);
        });
    }

    edit(userKey: string, user: User) {
        this.navController.push(
            UserEditComponent,
            {
                key: userKey,
                user: user
            }
        )
    }

    onSearch(event: any) {
        this.initializeItems();
        let val = event.target.value;
        if (val && val.trim() != '') {
            this.users = this.userService.getUsersFiltered(this.filter, val);
        }
    }

    onSearchCancel(event: any) {
        this.initializeItems();
    }

    initializeItems() {
        this.users = this.userService.getUsers();
    }

    getFilterTag(): string {
        switch (this.filter) {
            case this.Name:
                return "nombre";
            case this.Lastname:
                return "apellidos";
            case this.Email:
                return this.Email;
        }
    }
}
