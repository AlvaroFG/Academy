import { Component } from '@angular/core';
import { NavController, LoadingController, Loading, AlertController } from 'ionic-angular';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthProvider } from '../../providers/auth/auth';
import { LoginComponent } from '../login/login.component';
import { EmailValidator } from '../../validators/email.validator';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user.model';

@Component({
    selector: 'page-signup',
    templateUrl: 'signup.component.html'
})

export class SignupComponent {
    public signupForm: FormGroup;
    public loading: Loading;

    constructor(
        public navController: NavController,
        public authProvider: AuthProvider,
        public loadingController: LoadingController,
        public alertController: AlertController,
        public userService: UserService
    ) {
        this.signupForm = new FormGroup({
            email: new FormControl('', [Validators.compose(
                [Validators.required, Validators.maxLength(25), EmailValidator.isValid])
            ]),
            password: new FormControl('', [Validators.required, Validators.minLength(6)]),
            repeat: new FormControl('', [Validators.required, Validators.minLength(6)]),
            name: new FormControl('', [Validators.required, Validators.maxLength(15)]),
            lastname: new FormControl('', [Validators.required, Validators.maxLength(25)]),
            birthdate: new FormControl('', [Validators.required])
        }, this.passwordMatchValidator);
    }

    signupUser() {
        if (!this.signupForm.valid) {
            let alert = this.alertController.create({
                message: "Compruebe que todos los campos estén rellenos correctamente",
                buttons: [{
                    text: "Ok",
                    role: 'cancel'
                }]
            });
            alert.present();
        }
        else {
            this.authProvider.signupUser(this.signupForm.value.email, this.signupForm.value.password)
                .then(() => {
                    var authData = {
                        name: this.signupForm.value.name,
                        lastname: this.signupForm.value.lastname,
                        email: this.signupForm.value.email,
                        birthdate: this.signupForm.value.birthdate
                    }
                    var newUser: User = new User(authData);
                    this.userService.postUser(newUser);
                    this.navController.setRoot(LoginComponent);
                    let alert = this.alertController.create({
                        message: "Usuario registrado correctamente",
                        buttons: [{
                            text: "Ok",
                            role: 'cancel'
                        }]
                    });
                    alert.present();
                }, (error) => {
                    this.loading.dismiss().then(() => {
                        var errorMessage: string = error.message;
                        let alert = this.alertController.create({
                            message: errorMessage,
                            buttons: [{
                                text: "Ok",
                                role: 'cancel'
                            }]
                        });
                        alert.present();
                    });
                });

            this.loading = this.loadingController.create({
                dismissOnPageChange: true
            });
            this.loading.present();
        }
    }

    private passwordMatchValidator(group: FormGroup){
        return group.get('password').value === group.get('repeat').value
            ? null : {'mismatch': true};
    }
}