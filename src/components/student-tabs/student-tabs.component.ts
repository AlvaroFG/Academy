import { Component } from '@angular/core';
import { StudentSubjectListComponent } from '../../components/student-tabs-components/student-subjects-list/student-subjects-list.component';
import { StudentProfileComponent } from '../../components/student-tabs-components/student-profile/student-profile.component';

@Component({
  templateUrl: 'student-tabs.component.html'
})
export class StudentTabsComponent {

  tab1Root = StudentSubjectListComponent;
  tab2Root = StudentProfileComponent;

  constructor() {
  }
}
