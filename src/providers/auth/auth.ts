import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { App } from 'ionic-angular';
import { LoginComponent } from '../../components/login/login.component';

@Injectable()
export class AuthProvider {
    constructor (
        public angularFireAuth: AngularFireAuth,
        public app: App
    ) {

    }

    loginUser(email: string, password: string) : Promise<any> {
        return this.angularFireAuth.auth.signInWithEmailAndPassword(email, password);
    }

    resetPassword(email: string) : Promise<void> {
        return this.angularFireAuth.auth.sendPasswordResetEmail(email);
    }

    logoutUser() : Promise<void> {
        var nav = this.app.getRootNav();
        nav.setRoot(LoginComponent);
        return this.angularFireAuth.auth.signOut();
    }

    signupUser(newEmail: string, newPassword: string) : Promise<any> {
        return this.angularFireAuth.auth.createUserWithEmailAndPassword(newEmail, newPassword);
    }

    getLoggedUser() : string {
        return this.angularFireAuth.auth.currentUser.email;
    }

    removeUserAccount() {
        this.angularFireAuth.authState.subscribe(user => {
            user.delete();
        }).unsubscribe();
    }
}