import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { User } from '../models/user.model';
import { DatabaseReferences } from '../common/database-references';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class UserService {
    private usersRef: AngularFireList<User>;
    private users;

    constructor(private db: AngularFireDatabase) {
        this.initializeReference();
    }

    getUser(email: string) : Observable<User[]> {
        this.usersRef = this.db.list(DatabaseReferences.Users,
            ref => ref.orderByChild('email').equalTo(email));

        return this.usersRef.snapshotChanges().map(actions => {
            return actions.map(action => ({ key: action.key, ...action.payload.val() }));
          });
    }

    getUsers() {
        this.initializeReference();
        this.users = this.usersRef.snapshotChanges().map(changes => {
            return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
        });
        return this.users;
    }

    getUsersFiltered(child: string, value: string) {
        this.usersRef = this.db.list(DatabaseReferences.Users,
            ref => ref.orderByChild(child).equalTo(value));
        this.users = this.usersRef.snapshotChanges().map(changes => {
            return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
        });
        return this.users;
    }

    postUser(user: User) {
        this.initializeReference();
        this.usersRef.push(user);
    }

    updateUser(userKey: string, user: User) : Promise<void> {
        this.initializeReference();
        return this.usersRef.update(userKey, user);
    }

    async deleteUser(userKey: string, user: User) : Promise<void> {
        this.initializeReference();
        user.deleted = true;
        await this.delay(450);
        return this.usersRef.remove(userKey);
    }

    private initializeReference() {
        this.usersRef = this.db.list(DatabaseReferences.Users);
    }

    private delay(ms: number) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
}