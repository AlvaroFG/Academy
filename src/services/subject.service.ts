import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Subject } from '../models/subject.model';
import { DatabaseReferences } from '../common/database-references';

@Injectable()
export class SubjectService {
    private subjectsRef: AngularFireList<Subject>;
    private subjects;

    constructor(private db: AngularFireDatabase) {
        this.initializeReference();
    }

    getSubjects() {
        this.initializeReference();
        this.subjects = this.subjectsRef.snapshotChanges().map(changes => {
            return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
        });
        return this.subjects;
    }

    getSubjectsFiltered(type: string) {
        this.subjectsRef = this.db.list(DatabaseReferences.Subjects,
            ref => ref.orderByChild('type').equalTo(type));
        this.subjects = this.subjectsRef.snapshotChanges().map(changes => {
            return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
        });
        return this.subjects;
    }

    getSubjectsByName(name: string) {
        this.subjectsRef = this.db.list(DatabaseReferences.Subjects,
            ref => ref.orderByChild('name').equalTo(name));
        this.subjects = this.subjectsRef.snapshotChanges().map(changes => {
            return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
        });
        return this.subjects;
    }

    postSubject(subject: Subject) : PromiseLike<void> {
        this.initializeReference();
        return this.subjectsRef.push(subject);
    }

    updateSubject(subjectKey: string, subject: Subject) : Promise<void> {
        this.initializeReference();
        return this.subjectsRef.update(subjectKey, subject);
    }

    async deleteSubject(subjectKey: string, subject: Subject) : Promise<void> {
        this.initializeReference();
        subject.deleted = true;
        await this.delay(450);
        return this.subjectsRef.remove(subjectKey);
    }

    private initializeReference() {
        this.subjectsRef = this.db.list(DatabaseReferences.Subjects);
    }

    private delay(ms: number) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
}