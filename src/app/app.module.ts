import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { ImagePicker } from '@ionic-native/image-picker';
import { Camera } from '@ionic-native/camera';
import { Diagnostic } from '@ionic-native/diagnostic';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabase } from "angularfire2/database";
import { AngularFireAuth } from 'angularfire2/auth';
import { firebaseConfig } from '../environment';

import { AuthProvider } from '../providers/auth/auth';
import { UserService } from '../services/user.service';
import { SubjectService } from '../services/subject.service';

import { LoginComponent } from '../components/login/login.component';
import { ResetPasswordComponent } from '../components/reset-password/reset-password.component';
import { SignupComponent } from '../components/signup/signup.component';
import { TabsComponent } from '../components/tabs/tabs.component';
import { UserListComponent } from '../components/tabs-components/users-list/users-list.component';
import { SubjectListComponent } from '../components/tabs-components/subjects-list/subjects-list.component';
import { SubjectFormComponent } from '../components/subject-form/subject-form.component';

import { UserEditComponent } from '../components/tabs-components/users-list/user-edit/user-edit.component';

import { StudentTabsComponent } from '../components/student-tabs/student-tabs.component';
import { StudentSubjectListComponent } from '../components/student-tabs-components/student-subjects-list/student-subjects-list.component';
import { SubjectDetailComponent } from '../components/student-tabs-components/subject-detail/subject-detail.component';
import { StudentProfileComponent } from '../components/student-tabs-components/student-profile/student-profile.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    LoginComponent,
    ResetPasswordComponent,
    SignupComponent,
    TabsComponent,
    UserListComponent,
    SubjectListComponent,
    SubjectFormComponent,
    StudentTabsComponent,
    UserEditComponent,
    StudentSubjectListComponent,
    SubjectDetailComponent,
    StudentProfileComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginComponent,
    ResetPasswordComponent,
    SignupComponent,
    TabsComponent,
    UserListComponent,
    SubjectListComponent,
    SubjectFormComponent,
    StudentTabsComponent,
    UserEditComponent,
    StudentSubjectListComponent,
    SubjectDetailComponent,
    StudentProfileComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AngularFireAuth,
    AngularFireDatabase,
    AuthProvider,
    UserService,
    SubjectService,
    ImagePicker,
    Camera,
    Diagnostic,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
