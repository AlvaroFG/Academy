import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { LoginComponent } from '../components/login/login.component';
import { TabsComponent } from '../components/tabs/tabs.component';
import { StudentTabsComponent } from '../components/student-tabs/student-tabs.component';

import { UserService } from '../services/user.service';

@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  rootPage: any = LoginComponent;

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    angularFireAuth: AngularFireAuth,
    private userService: UserService
  ) {
    const authObserver = angularFireAuth.authState.subscribe(user => {
      if (user) {
        this.userService.getUser(user.email)
            .subscribe(users => {
                let currentUser = users[0];
                if (!currentUser.roles.admin == undefined || !currentUser.roles.admin) {
                  this.rootPage = TabsComponent;
                }
                else {
                  this.rootPage = TabsComponent;
                }
              }).unsubscribe();
      }
      else {
        this.rootPage = LoginComponent;
      }
      authObserver.unsubscribe();
    });

    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}
