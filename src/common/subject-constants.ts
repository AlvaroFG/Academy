export class SubjectConstants {
    public static readonly ESO: string = "ESO";
    public static readonly GRADE: string = "GRADE";
    public static readonly POSTGRADE: string = "POSTGRADE";
    public static readonly BACCALAURATE: string = "BACCALAURATE";
    public static readonly EsoValue: string = "ESO";
    public static readonly GradeValue: string = "Grado";
    public static readonly PostgradeValue: string = "Postgrado";
    public static readonly BaccalaurateValue: string = "Bachillerato";

    public static getValue(type: string) : string {
        switch (type)
        {
            case this.ESO:
                return this.EsoValue;
            case this.BACCALAURATE:
                return this.BaccalaurateValue;
            case this.GRADE:
                return this.GradeValue;
            case this.POSTGRADE:
                return this.PostgradeValue;
        }
    }
}